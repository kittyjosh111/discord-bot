# discord-bot
A little discord bot for a small discord friend server. Mocks the admin.
---
To run:
1) Clone or download the repo files.
2) Navigate to the downloaded directory.
3) Get a discord bot token and put into the last line of the python script.
4) Ensure you have python3 and pip install.
5) Run ```pip install -r requirements.txt```. It is recommended to use a virtual environment if you want. 
6) Finally execute: ```python3 script.py``` in the terminal.